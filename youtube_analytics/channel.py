import json
from pathlib import Path

from googleapiclient.http import HttpRequest

from .mixins import YouTubeApiMixin


class Channel(YouTubeApiMixin):
    def __init__(self, channel_id: str):
        item: dict = self._get_channel_item(channel_id)

        self.__channel_id = channel_id
        self.title: str = item['snippet']['title']
        self.description: str = item['snippet']['description']
        self.subscribers_count = int(item['statistics']['subscriberCount'])
        self.video_count: int = int(item['statistics']['videoCount'])
        self.view_count: int = int(item['statistics']['viewCount'])
        self.url: str = f'https://www.youtube.com/channel/{self.channel_id}'

    def _get_channel_item(self, channel_id: str) -> dict:
        with self.get_service() as api:
            request: HttpRequest = api.channels().list(id=channel_id, part='snippet,statistics')
            response = self.execute_request(request)
            return response['items'][0]

    @property
    def channel_id(self) -> str:
        return self.__channel_id

    def print_info(self) -> None:
        item_info = self._get_channel_item(self.channel_id)
        print(json.dumps(item_info, indent=2, ensure_ascii=False))  # noqa:T201

    def to_json(self, file_path: str) -> None:
        item_info = self._get_channel_item(self.channel_id)
        with Path(file_path).open('w', encoding='utf-8') as f:
            json.dump(item_info, f, indent=2, ensure_ascii=False)

    def __str__(self):
        return f'Youtube-канал: {self.title}'

    def __lt__(self, other: 'Channel') -> bool:
        return self.subscribers_count < other.subscribers_count

    def __gt__(self, other: 'Channel') -> bool:
        return self.subscribers_count > other.subscribers_count

    def __add__(self, other: 'Channel') -> int:
        return self.subscribers_count + other.subscribers_count
