from googleapiclient.http import HttpRequest

from .mixins import YouTubeApiMixin


class Video(YouTubeApiMixin):
    def __init__(self, video_id: str):
        self.__video_id = video_id

        if item := self._get_video_item(video_id):
            self.title = item['snippet']['title']
            self.view_count = int(item['statistics']['viewCount'])
            self.like_count = int(item['statistics']['likeCount'])
        else:
            self.title = self.view_count = self.like_count = None

    def _get_video_item(self, video_id: str) -> dict | None:
        with self.get_service() as api:
            request: HttpRequest = api.videos().list(id=video_id, part='statistics,snippet')
            response = self.execute_request(request)
            if response['items']:
                return response['items'][0]

    def __str__(self) -> str:
        return self.title

    @property
    def video_id(self) -> str:
        return self.__video_id


class PLVideo(YouTubeApiMixin):
    def __init__(self, video_id: str, playlist_id: str):
        video = Video(video_id)
        item = self._get_playlist_item(playlist_id)

        self.video_title = video.title
        self.video_views = video.view_count
        self.video_likes = video.like_count
        self.playlist_title = item['snippet']['title']

    def _get_playlist_item(self, playlist_id: str) -> dict:
        with self.get_service() as api:
            request: HttpRequest = api.playlists().list(id=playlist_id, part='snippet')
            response = self.execute_request(request)
            return response['items'][0]

    def __str__(self) -> str:
        return f'{self.video_title} ({self.playlist_title})'
