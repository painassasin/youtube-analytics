import contextlib
import logging
import os

from dotenv import load_dotenv
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from googleapiclient.http import HttpRequest

load_dotenv()

YOUTUBE_API_SERVICE_NAME = 'youtube'
YOUTUBE_API_VERSION = 'v3'
YOUTUBE_API_KEY: str = os.getenv('YOUTUBE_API_KEY')

logger = logging.getLogger(__name__)


class YouTubeApiMixin:
    @classmethod
    @contextlib.contextmanager
    def get_service(cls):
        """Возвращает объект для работы с API ютуба."""
        with build(
            serviceName=YOUTUBE_API_SERVICE_NAME,
            version=YOUTUBE_API_VERSION,
            developerKey=YOUTUBE_API_KEY
        ) as service:
            yield service

    @staticmethod
    def execute_request(request: HttpRequest) -> dict:
        try:
            return request.execute()
        except HttpError as e:
            logger.error('Error: %s', e)  # noqa: G200
            raise RuntimeError('Failed to get response from google API')
