from datetime import timedelta
from typing import Iterable

import isodate
from googleapiclient.http import HttpRequest

from .mixins import YouTubeApiMixin


class PlayList(YouTubeApiMixin):
    def __init__(self, playlist_id: str):
        item = self._get_playlist_item(playlist_id)
        self.title = item['snippet']['title']
        self.url = f'https://www.youtube.com/playlist?list={playlist_id}'

        self._video_ids = self._get_playlist_video_ids(playlist_id)
        self._video_items = self.__get_video_items(*self._video_ids)

    def _get_playlist_item(self, playlist_id: str) -> dict:
        with self.get_service() as api:
            request: HttpRequest = api.playlists().list(id=playlist_id, part='snippet')
            response = self.execute_request(request)
            return response['items'][0]

    @property
    def total_duration(self) -> timedelta:
        total_duration = timedelta()
        for video in self._video_items:
            duration = self.__get_video_item_duration(video)
            total_duration += duration

        return total_duration

    def show_best_video(self) -> str | None:
        best_video_id: str | None = None
        max_like_count: int = 0
        for video in self._video_items:
            like_count = int(video['statistics']['likeCount'])
            if like_count >= max_like_count:
                max_like_count = like_count
                best_video_id = video['id']

        return f'https://youtu.be/{best_video_id}' if best_video_id else None

    def _get_playlist_video_ids(self, playlist_id: str) -> Iterable[str]:
        with self.get_service() as api:
            request: HttpRequest = api.playlistItems().list(playlistId=playlist_id, part='contentDetails')
            response = self.execute_request(request)

        return (video['contentDetails']['videoId'] for video in response['items'])

    def __get_video_items(self, *video_ids) -> list[dict]:
        with self.get_service() as api:
            request: HttpRequest = api.videos().list(part='contentDetails,statistics', id=','.join(video_ids))
            response = self.execute_request(request)

        return response['items']

    @staticmethod
    def __get_video_item_duration(video_item: dict) -> timedelta:
        iso_8601_duration = video_item['contentDetails']['duration']
        return isodate.parse_duration(iso_8601_duration)
